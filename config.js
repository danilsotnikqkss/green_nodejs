const { ...variables } = process.env;

export const config = {
    ...variables,
};
