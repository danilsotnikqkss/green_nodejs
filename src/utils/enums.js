export const USER_STATUSES = ['Active', 'Inactive'];
export const ORGANIZATION_STATUSES = ['Todo', 'In progress', 'Done'];
export const RESOURCES_STATUSES = ['Active', 'Inactive'];
export const AREAS_STATUSES = ['Active', 'Inactive'];
export const LOGS_STATUSES = ['Todo', 'In progress', 'Done'];
