import winston from "winston";
import expressWinston from "express-winston";
import path from "path";
import { config } from "../../config";

const { LOGS_DESTINATION, NODE_ENV } = config;

export default class Logger {
    // singleton
    constructor() {
        if (!!Logger.instance) {
            return Logger.instance;
        } else {
            this.setLogger();
            Logger.instance = this;
            return this;
        }
    }

    setLogger() {
        this.logger = winston.createLogger({
            format: winston.format.json(),
            colorize: true,
            timestamp: true,
            transports: [
                new winston.transports.File({
                    filename: path.resolve(LOGS_DESTINATION, "error.log"),
                    level: "error",
                    json: true,
                    maxsize: 5242880 /*5MB */,
                    maxFiles: 5,
                    handleExceptions: true,
                }),
                new winston.transports.File({
                    filename: path.resolve(LOGS_DESTINATION, "combined.log"),
                }),
            ],
        });

        if (NODE_ENV !== 'production') {
            this.logger.add(
                new winston.transports.Console({
                    format: winston.format.combine(winston.format.colorize(), winston.format.simple()),
                })
            );
        }
    }

    error(errorMsg) {
        const date = new Date();
        const y = date.getFullYear();
        let m = "" + date.getMonth();
        const d = "" + date.getDate();
        m = m.length < 2 ? "0" + m : m;

        const TimeStamp = `${y}-${m}-${d} ${date}`;
        this.logger.error({ TimeStamp, errorMsg });
    }
}

export const loggerMiddleware = expressWinston.logger({
    transports: [
        new winston.transports.Console({
            format: winston.format.combine(winston.format.colorize(), winston.format.simple()),
        }),
    ],
    format: winston.format.combine(winston.format.colorize(), winston.format.simple()),
    meta: true, // optional: control whether you want to log the meta data about the request (default to true)
    msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
    expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
    colorize: true, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
    ignoreRoute: function (req, res) {
        return false;
    }, // optional: allows to skip some log messages based on request and/or response
});

export const errorLoggerMiddleware = expressWinston.errorLogger({
    transports: [
        new winston.transports.Console({
            format: winston.format.combine(winston.format.colorize(), winston.format.json()),
        }),
    ],
    format: winston.format.combine(winston.format.colorize(), winston.format.json()),
});

winston.addColors({
    error: "red",
    warn: "yellow",
    info: "cyan",
    debug: "green",
});
