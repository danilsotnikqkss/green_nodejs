export class GeneralError extends Error {
    constructor(message) {
        super();
        this.message = message;
    }

    getCode() {
        if (this instanceof BadRequest) {
            return 400;
        }
        if (this instanceof NotFound) {
            return 404;
        }
        return 500;
    }
}

export class BadRequest extends GeneralError {}
export class NotFound extends GeneralError {}

export const handleErrorResponse = (res, err) => {
    console.log(err)
    return res
        .status(err.status ? err.status : 500)
        .send({
            error: {
                msg: err.payload ? err.payload : "Something went wrong",
            },
        });
};
