// libs
import express from "express";
import { createServer } from "http";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import cors from "cors";
import expressip from "express-ip";
import rateLimit from "express-rate-limit";

//tools
import { validateRequest, handleErrors, checkJwt, handleAuthError } from "./middlewares";
import Logger, { loggerMiddleware, errorLoggerMiddleware } from "./utils/logger";
import { config } from "../config";

//routes
import { initZohoRoutes } from "./routes/zoho";
import { initOrganizationRoutes } from "./routes/organizations";
import { initUsersRoutes } from "./routes/users";
import { initFarmsRoutes } from "./routes/farms";
import { initAreasRoutes } from "./routes/areas";
import { initResourcesRoutes } from "./routes/resources";
import { initLogsRoutes } from "./routes/logs";
import { initNotificationsRoutes } from "./routes/notifications";

//docs
import swaggerUi from "swagger-ui-express";
import swaggerSpec from "./swagger";

//db
import "./database";

export class Server {
    constructor() {
        try {
            // The order here is important. Do not change it!
            this.initialize();
            this.setWinstonLogger();
            this.initRoutesMiddlewares();
            this.initRoutes();
            this.setWinstonErrorLogger();
        } catch (err) {
            console.log(err);
        }
    }

    initRoutes = () => {
        try {
            console.log("initalizing Routes ");
            initZohoRoutes(this.router, this.logger, this.routesMiddlewares);
            initOrganizationRoutes(this.router, this.logger, this.routesMiddlewares);
            initUsersRoutes(this.router, this.logger, this.routesMiddlewares);
            initFarmsRoutes(this.router, this.logger, this.routesMiddlewares);
            initAreasRoutes(this.router, this.logger, this.routesMiddlewares);
            initResourcesRoutes(this.router, this.logger, this.routesMiddlewares);
            initLogsRoutes(this.router, this.logger, this.routesMiddlewares);
            initNotificationsRoutes(this.router, this.logger, this.routesMiddlewares);
        } catch (err) {
            console.log(err);
        }
    };

    initialize = () => {
        try {
            const app = express();
            app.use(bodyParser.json({ limit: "50mb", extended: true }));
            app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
            app.use(cookieParser());
            app.use(express.json());
            app.use(expressip().getIpInfoMiddleware);
            app.disable("etag");
            app.use(function (req, res, next) {
                res.header("Access-Control-Allow-Credentials", true);
                res.header("Access-Control-Allow-Origin", req.headers.origin); // TODO, in production change to the real origin
                res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,UPDATE,OPTIONS");
                res.header(
                    "Access-Control-Allow-Headers",
                    "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept"
                );
                next();
            });

            const limiter = rateLimit({
                windowMs: 1 * 60 * 1000, // 1 minutes
                max: 100, // limit each IP to 100 requests per windowMs
            });
            app.use(limiter);

            app.use(
                cors({
                    origin: function (origin, callback) {
                        return callback(null, true);
                    },
                    optionsSuccessStatus: 200,
                    credentials: true,
                })
            );
            const router = express.Router();
            this.router = router;
            this.app = app;

            app.use("/api/", checkJwt, handleAuthError, router);
            app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
            this.DEFAULT_PORT = config.PORT || 3002;
            this.httpServer = createServer(this.app);
        } catch (err) {
            console.log(err);
        }
    };

    listen = (callback) => {
        this.httpServer.listen(this.DEFAULT_PORT, () => callback(this.DEFAULT_PORT));
    };

    setWinstonLogger = () => {
        this.logger = new Logger();
        this.app.use(loggerMiddleware);
    };

    setWinstonErrorLogger = () => {
        this.app.use(errorLoggerMiddleware);
        this.app.use(handleErrors);
    };

    initRoutesMiddlewares = () => {
        try {
            this.routesMiddlewares = {
                validateRequest,
            };
        } catch (err) {
            console.log(err);
        }
    };
}
