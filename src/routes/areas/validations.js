import Joi from "joi";
import { AREAS_STATUSES } from "../../utils/enums";

export const createArea = Joi.object({
    name: Joi.string().required(),
    organization_ref: Joi.string().required(),
    farm_ref: Joi.string().required(),
    status: Joi.string().required().valid(...AREAS_STATUSES),
}).unknown(true);

export const updateArea = Joi.object({
    name: Joi.string(),
    organization_ref: Joi.string(),
    farm_ref: Joi.string(),
    status: Joi.string().valid(...AREAS_STATUSES),
}).unknown(true);


