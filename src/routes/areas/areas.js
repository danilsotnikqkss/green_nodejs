import { handleErrorResponse } from "../../utils/errors";
import * as validations from "./validations";

export default (router, service, logger, { validateRequest }) => {
    router.post("/area", validateRequest(validations.createArea), async (req, res) => {
        try {
            const _response = await service.createArea(req.body);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.put("/area/:id", validateRequest(validations.updateArea), async (req, res) => {
        try {
            const _response = await service.updateArea(req.params.id, req.body);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.get("/area/:id", async (req, res) => {
        try {
            const _response = await service.getArea(req.params.id);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
};
