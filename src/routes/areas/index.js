
//routes
import areas from './areas';

//services
import areasServices from '../../services/areas';

export const initAreasRoutes = (router, logger, middlewares)=>{
    areas(router, areasServices(logger), logger, middlewares);
}