import { handleErrorResponse } from "../../utils/errors";
import * as validations from "./validations";

export default (router, service, logger, { validateRequest }) => {
    router.post("/log", validateRequest(validations.createLog), async (req, res) => {
        try {
            const _response = await service.createLog(req.user.sub, req.body);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.put("/log/:id", validateRequest(validations.updateLog), async (req, res) => {
        try {
            const _response = await service.updateLog(req.user.sub, req.params.id, req.body);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.get("/log/:id", async (req, res) => {
        try {
            const _response = await service.getLog(req.params.id);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
};
