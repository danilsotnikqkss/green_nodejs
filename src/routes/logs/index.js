
//routes
import logs from './logs';

//services
import logsServices from '../../services/logs';

export const initLogsRoutes = (router, logger, middlewares)=>{
    logs(router, logsServices(logger), logger, middlewares);
}