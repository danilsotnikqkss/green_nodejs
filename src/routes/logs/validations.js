import Joi from "joi";
import { LOGS_STATUSES } from "../../utils/enums";

export const createLog = Joi.object({
    farm_ref: Joi.string().required(),
    area_ref: Joi.string().required(),
    temperature: Joi.number().required(),
    humidity: Joi.number().required(),
    lat: Joi.number().required(),
    lon: Joi.number().required(),
    co2:  Joi.number().required(),
    water_volume:  Joi.number().required(),
    notes: Joi.string().required(),
    attachment: Joi.string(),
    status: Joi.string().required().valid(...LOGS_STATUSES),
    lifecycle: Joi.string().required(),
    age: Joi.date().required(),
    health_status: Joi.string().required(),
    strain: Joi.string().required(),
}).unknown(true);

export const updateLog = Joi.object({
    farm_ref: Joi.string(),
    area_ref: Joi.string(),
    temperature: Joi.number(),
    humidity: Joi.number(),
    lat: Joi.number(),
    lon: Joi.number(),
    co2:  Joi.number(),
    water_volume:  Joi.number(),
    notes: Joi.string(),
    attachment: Joi.string(),
    status: Joi.string().valid(...LOGS_STATUSES),
    lifecycle: Joi.string(),
    age: Joi.date(),
    health_status: Joi.string(),
    strain: Joi.string(),
}).unknown(true);


