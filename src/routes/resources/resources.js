import { handleErrorResponse } from "../../utils/errors";
import * as validations from "./validations";

export default (router, service, logger, { validateRequest }) => {
    router.post("/resource", validateRequest(validations.createResource), async (req, res) => {
        try {
            const _response = await service.createResource(req.body);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.put("/resource/:id", validateRequest(validations.updateResource), async (req, res) => {
        try {
            const _response = await service.updateResource(req.params.id, req.body);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.get("/resource/:id", async (req, res) => {
        try {
            const _response = await service.getResource(req.params.id);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
};
