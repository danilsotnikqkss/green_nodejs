
//routes
import resources from './resources';

//services
import resourcesServices from '../../services/resources';

export const initResourcesRoutes = (router, logger, middlewares)=>{
    resources(router, resourcesServices(logger), logger, middlewares);
}