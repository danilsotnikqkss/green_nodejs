import Joi from "joi";
import { RESOURCES_STATUSES } from "../../utils/enums";

export const createResource = Joi.object({
    name: Joi.string().required(),
    type: Joi.string().required(),
    description: Joi.string().required(),
    status: Joi.string().required().valid(...RESOURCES_STATUSES),
    organization_ref: Joi.string().required(),
    area_ref: Joi.string().required(),
    key: Joi.string().required(),
    marker: Joi.string().required(),
    address: Joi.string().required(),
    node_id: Joi.number().required(),
    type_key: Joi.number().required(),
}).unknown(true);

export const updateResource = Joi.object({
    name: Joi.string(),
    type: Joi.string(),
    description: Joi.string(),
    status: Joi.string().valid(...RESOURCES_STATUSES),
    organization_ref: Joi.string(),
    area_ref: Joi.string(),
    key: Joi.string(),
    marker: Joi.string(),
    address: Joi.string(),
    node_id: Joi.number(),
    type_key: Joi.number(),
}).unknown(true);


