import Joi from "joi";

export const createNotification = Joi.object({
    user_ref: Joi.string().required(),
    title: Joi.string().required(),
    description: Joi.string().required(),
    badge: Joi.number().required(),
    viewed: Joi.boolean().required(),
    type: Joi.string().required(),
}).unknown(true);

export const updateNotification = Joi.object({
    user_ref: Joi.string(),
    title: Joi.string(),
    description: Joi.string(),
    badge: Joi.number(),
    viewed: Joi.boolean(),
    type: Joi.string(),
}).unknown(true);


