
//routes
import notifications from './notifications';

//services
import notificationsServices from '../../services/notifications';

export const initNotificationsRoutes = (router, logger, middlewares)=>{
    notifications(router, notificationsServices(logger), logger, middlewares);
}