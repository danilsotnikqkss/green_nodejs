import { handleErrorResponse } from "../../utils/errors";
import * as validations from "./validations";

export default (router, service, logger, { validateRequest }) => {
    router.post("/notification", validateRequest(validations.createNotification), async (req, res) => {
        try {
            const _response = await service.createNotification(req.body);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.put("/notification/:id", validateRequest(validations.updateNotification), async (req, res) => {
        try {
            const _response = await service.updateNotification(req.params.id, req.body);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.get("/notification/:id", async (req, res) => {
        try {
            const _response = await service.getNotification(req.params.id);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
};
