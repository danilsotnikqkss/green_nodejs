import zoho from './zoho';
import zohoService from '../../services/zoho';

export const initZohoRoutes = (router, logger, middlewares)=>{
    zoho(router, zohoService(logger), logger, middlewares);
}