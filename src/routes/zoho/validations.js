import Joi from "joi";

export const checkValidation = Joi.object({
    name: Joi.string().required(),
}).unknown(true);