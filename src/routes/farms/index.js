
//routes
import farms from './farms';

//services
import farmsServices from '../../services/farms';

export const initFarmsRoutes = (router, logger, middlewares)=>{
    farms(router, farmsServices(logger), logger, middlewares);
}