import Joi from "joi";
import { ORGANIZATION_STATUSES } from "../../utils/enums";

export const createFarm = Joi.object({
    name: Joi.string().required(),
    organization_ref: Joi.string().required(),
    lat: Joi.number().required(),
    lon: Joi.number().required(),
    notes: Joi.string().required(),
}).unknown(true);

export const updateFarm = Joi.object({
    name: Joi.string(),
    organization_ref: Joi.string(),
    lat: Joi.number(),
    lon: Joi.number(),
    notes: Joi.string(),
}).unknown(true);


