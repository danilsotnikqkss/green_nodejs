import { handleErrorResponse } from "../../utils/errors";
import * as validations from "./validations";

export default (router, service, logger, { validateRequest }) => {
    router.post("/farm", validateRequest(validations.createFarm), async (req, res) => {
        try {
            const _response = await service.createFarm(req.body);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.put("/farm/:id", validateRequest(validations.updateFarm), async (req, res) => {
        try {
            const _response = await service.updateFarm(req.params.id, req.body);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.get("/farm/:id", async (req, res) => {
        try {
            const _response = await service.getFarm(req.params.id);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
};
