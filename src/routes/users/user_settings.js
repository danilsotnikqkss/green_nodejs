import { handleErrorResponse } from "../../utils/errors";
import * as validations from "./validations";

export default (router, service, logger, { validateRequest }) => {
    router.post(
        "/user/:id/settings",
        validateRequest(validations.createOrUpdateUserSettings),
        async (req, res) => {
            try {
                const _response = await service.createUserSettings(req.params.id, req.body);
                return res.status(_response.status).send(_response.payload);
            } catch (err) {
                logger.error(err);
                return handleErrorResponse(res, err);
            }
        }
    );
    router.get("/user/:id/settings", async (req, res) => {
        try {
            const _response = await service.getUserSettings(req.params.id);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.put(
        "/user/:id/settings",
        validateRequest(validations.createOrUpdateUserSettings),
        async (req, res) => {
            try {
                const _response = await service.updateUserSettings(req.params.id, req.body);
                return res.status(_response.status).send(_response.payload);
            } catch (err) {
                logger.error(err);
                return handleErrorResponse(res, err);
            }
        }
    );
};
