import Joi from "joi";
import { USER_STATUSES } from "../../utils/enums";

export const createUser = Joi.object({
    first_name: Joi.string().required(),
    last_name: Joi.string().required(),
    email: Joi.string().required(),
    auth0_id: Joi.string().required(),
    organization_ref: Joi.string().required(),
    status: Joi.string().valid(...USER_STATUSES),
}).unknown(true);

export const updateUser = Joi.object({
    first_name: Joi.string(),
    last_name: Joi.string(),
    email: Joi.string(),
    auth0_id: Joi.string(),
    organization_ref: Joi.string(),
    status: Joi.string().valid(...USER_STATUSES),
}).unknown(true);

export const createOrUpdateUserSettings = Joi.object({
    chat: Joi.boolean(),
    forms: Joi.boolean(),
    today_at_a_glance: Joi.boolean(),
    daily_log: Joi.boolean(),
    fetrilizer_log: Joi.boolean(),
    treatment_log: Joi.boolean(),
    to_do_list: Joi.boolean(),
    chat_setting: Joi.boolean(),
}).unknown(true);

