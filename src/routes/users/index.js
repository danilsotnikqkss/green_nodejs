
//routes
import users from './users';
import userSettings from './user_settings';

//services
import * as userServices from '../../services/users';

export const initUsersRoutes = (router, logger, middlewares)=>{
    users(router, userServices.users(logger), logger, middlewares);
    userSettings(router, userServices.settings(logger), logger, middlewares);
}