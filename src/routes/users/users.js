import { handleErrorResponse } from "../../utils/errors";
import * as validations from "./validations";

export default (router, service, logger, { validateRequest }) => {
    router.post("/user", validateRequest(validations.createUser), async (req, res) => {
        try {
            const _response = await service.createUser(req.body);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.put("/user/:id", validateRequest(validations.updateUser), async (req, res) => {
        try {
            const _response = await service.updateUser(req.params.id, req.body);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.get("/user/:id", async (req, res) => {
        try {
            const _response = await service.getUser(req.params.id);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
};
