import { handleErrorResponse } from "../../utils/errors";
import * as validations from "./validations";

export default (router, service, logger, { validateRequest }) => {
    router.post(
        "/organization",
        validateRequest(validations.createOrganization),
        async (req, res) => {
            try {
                const _response = await service.createOrganization(req.body);
                return res.status(_response.status).send(_response.payload);
            } catch (err) {
                logger.error(err);
                return handleErrorResponse(res, err);
            }
        }
    );
    router.get("/organization/:id", async (req, res) => {
        try {
            const _response = await service.getOrganizationById(req.params.id);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.get("/organizations", async (req, res) => {
        try {
            const _response = await service.getAllOrganizations(req.params.id);
            return res.status(_response.status).send(_response.payload);
        } catch (err) {
            logger.error(err);
            return handleErrorResponse(res, err);
        }
    });
    router.put(
        "/organization/:id",
        validateRequest(validations.updateOrganization),
        async (req, res) => {
            try {
                const _response = await service.updateOrganizationById(req.params.id, req.body);
                return res.status(_response.status).send(_response.payload);
            } catch (err) {
                logger.error(err);
                return handleErrorResponse(res, err);
            }
        }
    );
};
