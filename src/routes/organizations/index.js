
import organizations from './organizations';
import organizationsService from '../../services/organizations';

export const initOrganizationRoutes = (router, logger, middlewares)=>{
    organizations(router, organizationsService(logger), logger, middlewares);
}