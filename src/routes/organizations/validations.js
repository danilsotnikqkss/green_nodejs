import Joi from "joi";
import { ORGANIZATION_STATUSES } from "../../utils/enums";

export const createOrganization = Joi.object({
    name: Joi.string().required(),
    address: Joi.string().required(),
    owner: Joi.string().required(),
    email: Joi.string().email().required(),
    phone: Joi.string().required(),
    status: Joi.string().valid(...ORGANIZATION_STATUSES),
}).unknown(true);

export const updateOrganization = Joi.object({
    name: Joi.string(),
    address: Joi.string(),
    owner: Joi.string(),
    email: Joi.string().email(),
    phone: Joi.string(),
    status: Joi.string().valid(...ORGANIZATION_STATUSES),
}).unknown(true);

