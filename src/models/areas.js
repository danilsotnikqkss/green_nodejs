import mongoose from "mongoose";
import { AREAS_STATUSES } from "../utils/enums";

const AreasSchema = mongoose.Schema(
    {
        id: mongoose.Schema.ObjectId,
        organization_ref: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Organizations'
        },
        farm_ref: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Farms'
        },
        name:{ type: String, required: true },
        status: { type: String, enum: AREAS_STATUSES },
    },
    {
        collection: "areas",
    }
);

export default mongoose.model("Areas", AreasSchema);
