import mongoose from "mongoose";
import { LOGS_STATUSES } from "../utils/enums";

const LogsSchema = mongoose.Schema(
    {
        id: mongoose.Schema.ObjectId,
        area_ref: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Areas",
        },
        farm_ref: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Farms",
        },
        temperature: { type: Number, required: true },
        humidity: { type: Number, required: true },
        lat: { type: Number, required: true },
        lon: { type: Number, required: true },
        co2: { type: Number, required: true },
        water_volume: { type: Number, required: true },
        notes: { type: String, required: true },
        attachment: { type: String },
        status: { type: String, enum: LOGS_STATUSES },
        create_user_ref: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Users",
        },
        update_user_ref: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Users",
        },
        createdAt: { type: Date },
        updatedAt: { type: Date },
        lifecycle: { type: String, required: true },
        age: { type: Date, required: true },
        health_status: { type: String, required: true },
        strain: { type: String, required: true },
    },
    {
        collection: "logs",
    }
);

export default mongoose.model("Logs", LogsSchema);
