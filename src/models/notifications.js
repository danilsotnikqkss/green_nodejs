import mongoose from "mongoose";

const NotificationsSchema = mongoose.Schema(
    {
        id: mongoose.Schema.ObjectId,
        user_ref: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Users",
        },
        title: { type: String, required: true },
        description: { type: String, required: true },
        badge: { type: Number, required: true },
        createdAt: { type: Date, required: true },
        viewed: { type: Boolean, required: true },
        type: { type: String, required: true },
    },
    {
        collection: "notifications",
    }
);

export default mongoose.model("Notifications", NotificationsSchema);
