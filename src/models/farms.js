import mongoose from "mongoose";

const FarmsSchema = mongoose.Schema(
    {
        id: mongoose.Schema.ObjectId,
        organization_ref: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Organizations'
        },
        name:{ type: String, required: true },
        lat:{ type: Number, required: true },
        lon:{ type: Number, required: true },
        notes:{ type: String, required: true },
    },
    {
        collection: "farms",
    }
);

export default mongoose.model("Farms", FarmsSchema);
