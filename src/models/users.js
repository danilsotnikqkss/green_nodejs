import mongoose from "mongoose";
import { USER_STATUSES } from "../utils/enums";

const UsersSchema = mongoose.Schema(
    {
        id: mongoose.Schema.ObjectId,
        auth0_id: { type: String, required: true },
        first_name: { type: String, required: true },
        last_name: { type: String, required: true },
        email: { type: String, required: true, unique: true, dropDups: true },
        status: { type: String, enum: USER_STATUSES },
        organization_ref: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Organizations'
        },
        settings_ref: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'UserSettings'
        }
    },
    {
        collection: "users",
    }
);

export default mongoose.model("Users", UsersSchema);
