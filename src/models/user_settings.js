import mongoose from "mongoose";

const UserSettingsSchema = mongoose.Schema(
    {
        id: mongoose.Schema.ObjectId,
        user_ref: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users'
        },
        chat: { type: Boolean, default: true },
        forms: { type: Boolean, default: true },
        today_at_a_glance: { type: Boolean, default: true },
        daily_log: { type: Boolean, default: true },
        fetrilizer_log: { type: Boolean, default: true },
        treatment_log: { type: Boolean, default: true },
        to_do_list: { type: Boolean, default: true },
        chat_setting: { type: Boolean, default: true },
    },
    {
        collection: "user_settings",
    }
);

export default mongoose.model("UserSettings", UserSettingsSchema);
