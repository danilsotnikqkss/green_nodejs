import mongoose from "mongoose";
import { RESOURCES_STATUSES } from "../utils/enums";

const ResourcesSchema = mongoose.Schema(
    {
        id: mongoose.Schema.ObjectId,
        name: { type: String, required: true },
        type: { type: String, required: true },
        description: { type: String, required: true },
        status: { type: String, enum: RESOURCES_STATUSES },
        organization_ref: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Organizations'
        },
        area_ref: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Areas'
        },
        key:{ type: String, required: true },
        marker:{ type: String, required: true },
        address:{ type: String, required: true },
        node_id:{ type: Number, required: true },
        type_key:{ type: Number, required: true },
    },
    {
        collection: "resources",
    }
);

export default mongoose.model("Resources", ResourcesSchema);
