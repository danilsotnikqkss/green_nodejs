import mongoose from "mongoose";
import { ORGANIZATION_STATUSES } from "../utils/enums";

const OrganizationsSchema = mongoose.Schema(
    {
        id: mongoose.Schema.ObjectId,
        name: { type: String, required: true },
        address: { type: String, required: true },
        owner: { type: String, required: true },
        email: { type: String, required: true, unique: true, dropDups: true },
        phone: { type: String, required: true },
        status: { type: String, enum: ORGANIZATION_STATUSES, default: "Todo" },
    },
    {
        collection: "organizations",
    }
);

export default mongoose.model("Organizations", OrganizationsSchema);
