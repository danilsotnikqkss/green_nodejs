import mongoose from 'mongoose';
import { config } from '../config';

mongoose.connect(config.MONGO_CONNECTION_LINK, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});