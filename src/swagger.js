import swaggerJSDoc from "swagger-jsdoc";

const swaggerDefinition = {
    info: {
        title: "API for the Green Screens",
        version: "1.0.0",
    },
    host: "localhost:3031",
    basePath: "/api",
    securityDefinitions: {
        Bearer: {
            type: "apiKey",
            name: "Authorization",
            in: "header",
            scheme: "bearer",
            bearerFormat: "JWT",
        },
    },
};

const options = {
    swaggerDefinition,
    apis: ["./docs/**/*.yaml"],
};
export default swaggerJSDoc(options);
