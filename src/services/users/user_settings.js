import { Users, UserSettings } from "../../models";

export default (logger) => {
    return {
        createUserSettings: (userId, data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const user = await Users.findById(userId);
                    if (!user) {
                        return reject({
                            status: 400,
                            payload: "The user wasn't found",
                        });
                    }
                    const isUserSettingsExist = await UserSettings.findOne({
                        user_ref: userId,
                    });
                    if (isUserSettingsExist) {
                        return reject({
                            status: 400,
                            payload: "The settings for provided user already exist",
                        });
                    }
                    const userSettings = await UserSettings.create({ ...data, user_ref: userId });
                    return resolve({ status: 201, payload: userSettings });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        getUserSettings: (userId) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const userSettings = await UserSettings.findOne({
                        user_ref: userId,
                    });

                    if (!userSettings) {
                        return reject({
                            status: 400,
                            payload: "The settings for provided user wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: userSettings });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        updateUserSettings: (userId, data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const { user_ref, ...rest } = data;
                    const userSettings = await UserSettings.findOneAndUpdate(
                        { user_ref: userId },
                        rest,
                        { new: true }
                    );

                    if (!userSettings) {
                        return reject({
                            status: 400,
                            payload: "The settings for provided user wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: userSettings });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
    };
};
