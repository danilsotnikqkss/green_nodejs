import { Users, UserSettings } from "../../models";
import organizationService from "../organizations";

export default (logger) => {
    return {
        createUser: (data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const isUserExist = await Users.findOne({
                        email: data.email,
                    });
                    if (isUserExist) {
                        return reject({
                            status: 400,
                            payload: "The user with provided email already exist",
                        });
                    }
                    await organizationService().getOrganizationById(data.organization_ref);

                    const tempUser = await Users.create(data);
                    const settings = await UserSettings.create({ user_ref: tempUser._id });

                    const user = await Users.findOneAndUpdate(
                        { _id: tempUser._id },
                        { settings_ref: settings._id },
                        { new: true }
                    );

                    return resolve({ status: 201, payload: user });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        updateUser: (userId, data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const { settings_ref, organization_ref, _id, ...rest } = data;
                    const user = await Users.findOneAndUpdate({ _id: userId }, rest, { new: true });
                    if (!user) {
                        return reject({
                            status: 400,
                            payload: "The user with provided id wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: user });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        getUser: (userId) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const user = await Users.findById(userId).populate('settings_ref').populate('organization_ref');
                    if (!user) {
                        return reject({
                            status: 400,
                            payload: "The user with provided id wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: user });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
    };
};
