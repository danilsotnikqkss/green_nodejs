import { Organizations } from "../../models";

export default (logger) => {
    return {
        createOrganization: (data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const isOrganizationExist = await Organizations.findOne({
                        email: data.email,
                    });
                    if (isOrganizationExist) {
                        return reject({
                            status: 400,
                            payload: "The organization with provided email already exist",
                        });
                    }
                    const organization = await Organizations.create(data);
                    return resolve({ status: 201, payload: organization });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        getOrganizationById: (organizationId) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const organization = await Organizations.findOne({ _id: organizationId });
                    if (!organization) {
                        return reject({
                            status: 400,
                            payload: "The organization wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: organization });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        updateOrganizationById: (organizationId, data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const organization = await Organizations.findOneAndUpdate(
                        { _id: organizationId },
                        data,
                        { new: true }
                    );
                    if (!organization) {
                        return reject({
                            status: 400,
                            payload: "The organization wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: organization });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        getAllOrganizations: () => {
            return new Promise(async (resolve, reject) => {
                try {
                    const organizations = await Organizations.find();
                    return resolve({ status: 200, payload: organizations });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
    };
};
