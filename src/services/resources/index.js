import { Resources } from "../../models";
import organizationService from "../organizations";
import areasService from "../areas";

export default (logger) => {
    return {
        createResource: (data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    await organizationService().getOrganizationById(data.organization_ref);
                    await areasService().getArea(data.area_ref);
                    const resource = await Resources.create(data);
                    return resolve({ status: 201, payload: resource });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        updateResource: (resourceId, data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const { organization_ref, area_ref, _id, ...rest } = data;
                    const resource = await Resources.findOneAndUpdate({ _id: resourceId }, rest, { new: true });
                    if (!resource) {
                        return reject({
                            status: 400,
                            payload: "The resource with provided id wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: resource });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        getResource: (resourceId) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const resource = await Resources.findById(resourceId).populate('organization_ref').populate('area_ref');
                    if (!resource) {
                        return reject({
                            status: 400,
                            payload: "The resource with provided id wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: resource });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
    };
};
