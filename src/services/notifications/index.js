import { Notifications } from "../../models";
import userService from "../users/users";

export default (logger) => {
    return {
        createNotification: (data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const { createdAt, ...rest } = data;
                    await userService().getUser(rest.user_ref);

                    const notification = await Notifications.create({
                        ...rest,
                        createdAt: new Date(),
                    });
                    return resolve({ status: 201, payload: notification });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        getNotification: (notificationId) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const notification = await Notifications.findOne({ _id: notificationId });
                    if (!notification) {
                        return reject({
                            status: 400,
                            payload: "The notification wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: notification });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        updateNotification: (notificationId, data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const { user_ref, createdAt, ...rest } = data;

                    const notification = await Notifications.findOneAndUpdate(
                        { _id: notificationId },
                        rest,
                        { new: true }
                    );
                    if (!notification) {
                        return reject({
                            status: 400,
                            payload: "The notification wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: notification });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
    };
};
