import { Areas } from "../../models";
import organizationService from "../organizations";
import farmsService from "../farms";

export default (logger) => {
    return {
        createArea: (data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    await organizationService().getOrganizationById(data.organization_ref);
                    await farmsService().getFarm(data.farm_ref);
                    const farm = await Areas.create(data);
                    return resolve({ status: 201, payload: farm });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        updateArea: (areaId, data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const { organization_ref, farm_ref, _id, ...rest } = data;
                    const area = await Areas.findOneAndUpdate({ _id: areaId }, rest, { new: true });
                    if (!area) {
                        return reject({
                            status: 400,
                            payload: "The area with provided id wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: area });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        getArea: (areaId) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const area = await Areas.findById(areaId).populate('organization_ref').populate('farm_ref');
                    if (!area) {
                        return reject({
                            status: 400,
                            payload: "The area with provided id wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: area });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
    };
};
