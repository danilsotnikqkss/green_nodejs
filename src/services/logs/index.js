import { Logs, Users } from "../../models";
import areaService from "../areas";
import farmService from "../farms";

export default (logger) => {
    return {
        createLog: (userId, data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const { createdAt, updatedAt, create_user_ref, update_user_ref, ...rest } =
                        data;
                    await areaService().getArea(data.area_ref);
                    await farmService().getFarm(data.farm_ref);

                    const user = await Users.findOne({ auth0_id: userId });
                    if (!user) {
                        return reject({
                            status: 400,
                            payload: "The user wasn't found",
                        });
                    }
                    const log = await Logs.create({
                        ...rest,
                        create_user_ref: user._id,
                        createdAt: new Date(),
                    });
                    return resolve({ status: 201, payload: log });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        getLog: (logId) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const log = await Logs.findOne({ _id: logId })
                        .populate("create_user_ref")
                        .populate("update_user_ref");
                    if (!log) {
                        return reject({
                            status: 400,
                            payload: "The log wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: log });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        updateLog: (userId, logId, data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const {
                        createdAt,
                        updatedAt,
                        create_user_ref,
                        update_user_ref,
                        area_ref,
                        farm_ref,
                        ...rest
                    } = data;
                    const user = await Users.findOne({ auth0_id: userId });
                    if (!user) {
                        return reject({
                            status: 400,
                            payload: "The user wasn't found",
                        });
                    }
                    const log = await Logs.findOneAndUpdate(
                        { _id: logId },
                        { ...rest, updatedAt: new Date(), update_user_ref: user._id },
                        { new: true }
                    );
                    if (!log) {
                        return reject({
                            status: 400,
                            payload: "The log wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: log });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
    };
};
