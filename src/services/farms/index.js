import { Farms } from "../../models";
import organizationService from "../organizations";

export default (logger) => {
    return {
        createFarm: (data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    await organizationService().getOrganizationById(data.organization_ref);
                    const farm = await Farms.create(data);
                    return resolve({ status: 201, payload: farm });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        updateFarm: (farmId, data) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const { organization_ref, _id, ...rest } = data;
                    const farm = await Farms.findOneAndUpdate({ _id: farmId }, rest, { new: true });
                    if (!farm) {
                        return reject({
                            status: 400,
                            payload: "The farm with provided id wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: farm });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
        getFarm: (farmId) => {
            return new Promise(async (resolve, reject) => {
                try {
                    const farm = await Farms.findById(farmId).populate('organization_ref');
                    if (!farm) {
                        return reject({
                            status: 400,
                            payload: "The farm with provided id wasn't found",
                        });
                    }
                    return resolve({ status: 200, payload: farm });
                } catch (err) {
                    return reject({ status: 400, payload: err });
                }
            });
        },
    };
};
