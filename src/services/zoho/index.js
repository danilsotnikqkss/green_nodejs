
export default (logger)=>{
    return {
        testMethod: ()=>{
            return new Promise(async(resolve, reject)=>{
                try{
                    const data = [{id: 1, name: 'test'}]
                    return resolve({status: 200, payload: data})
                }catch(err){
                    logger.error(err);
                    return reject({status: 500, payload: err});
                }
            })
        }
    }
}