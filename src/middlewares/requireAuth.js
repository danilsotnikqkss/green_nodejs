import jwt from "express-jwt";
import jwksRsa from "jwks-rsa";
import { config } from "../../config";
import { publicRoutes } from "../utils/publicRoutes";

export const checkJwt = jwt({
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: `https://${config.AUTH0_DOMAIN}/.well-known/jwks.json`,
    }),

    audience: config.AUTH0_AUDIENCE,
    issuer: `https://${config.AUTH0_DOMAIN}/`,
    algorithms: [config.AUTH0_ALGORITHM],
}).unless(publicRoutes);

export const handleAuthError = (err, req, res, next) => {
    if (err.name === "UnauthorizedError") {
        return res.status(401).send({ error: "Unauthorized" });
    }
    next();
};
