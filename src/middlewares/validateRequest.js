export default (validationSchema = null) => {
    return function (req, res, next) {
        if (validationSchema) {
            const result = validationSchema.validate(req.body);

            if (result.error) {
                return res
                    .status(400)
                    .send(result.error.details ? result.error.details : result.error);
            }
        }
        next();
    };
};
