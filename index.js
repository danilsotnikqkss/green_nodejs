import { Server } from "./src/server";

const server = new Server();
 
server.listen(port => {
 console.log(`Gateway service is listening on port ${port}`);
});